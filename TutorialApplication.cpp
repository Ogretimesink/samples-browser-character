/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	// Set the viewport background color
	mCamera->getViewport()->setBackgroundColour(ColourValue(1.0f, 1.0f, 0.8f));

	// Create a linear type fog
	mSceneMgr->setFog(Ogre::FOG_LINEAR, ColourValue(1.0f, 1.0f, 0.8f), 0, 15, 100);

	// Position the camera
	mCamera->setPosition(Ogre::Vector3(0, 0, 15));

	// Look at a position
	mCamera->lookAt(Ogre::Vector3(0, 0, 0));

	// Set the method to generate shadows in the environment 
	mSceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_MODULATIVE);

	// Set the color of shadows
	mSceneMgr->setShadowColour(Ogre::ColourValue(0.5f, 0.5f, 0.5f));

	// Set the level of shadow detail
	mSceneMgr->setShadowTextureSize(1024);

	// Number of textures used for shadows
	mSceneMgr->setShadowTextureCount(1);

	// Disable default camera control
	mCameraMan->setStyle(OgreBites::CS_MANUAL);

	// Add light which illuminates all objects in the scene regardless of direction
	mSceneMgr->setAmbientLight(ColourValue(0.3f, 0.3f, 0.3f));

	// Add a light that gives shadows
	Ogre::Light* light = mSceneMgr->createLight();

	// Set to light that gives light equally in all directions
	light->setType(Light::LT_POINT);

	// Set the position of the light
	light->setPosition(-10, 40, 20);

	// Set the shininess of light that reflects off scene models
	light->setSpecularColour(ColourValue::White);

	// Create a plane object and make a mesh out of it
	Ogre::MeshManager::getSingleton().createPlane("floor", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
		Plane(Vector3::UNIT_Y, 0), 100, 100, 10, 10, true, 1, 10, 10, Vector3::UNIT_Z);

	// Create an instance of a computer graphic model using the plane mesh
	Ogre::Entity* floor = mSceneMgr->createEntity("Floor", "floor");

	// Set the material for the model
	floor->setMaterialName("Examples/Rockwall");

	// Don't allow the entity to cast shadows on its surrounding environment
	floor->setCastShadows(false);

	// Create a node and add the entity directly to it 
	mSceneMgr->getRootSceneNode()->attachObject(floor);

	// Create the sinbad character controller
	mChara = new SinbadCharacterController(mCamera);
}
//---------------------------------------------------------------------------
void TutorialApplication::createFrameListener(void)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::createFrameListener();
}
//---------------------------------------------------------------------------
void TutorialApplication::destroyScene(void)
{
	// Clean up character controller
	if (mChara) delete mChara;

	// Clean up the floor
	MeshManager::getSingleton().remove("floor");
}
//---------------------------------------------------------------------------
bool TutorialApplication::frameRenderingQueued(const Ogre::FrameEvent& fe)
{
	// Call the BaseApplication virtual function because we still want its functionality
	bool ret = BaseApplication::frameRenderingQueued(fe);

	// Update character animations and the camera
	mChara->addTime(fe.timeSinceLastFrame);

	return ret;
}
//---------------------------------------------------------------------------
bool TutorialApplication::keyPressed(const OIS::KeyEvent& ke)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::keyPressed(ke);

	// Relay keyboard input events to character controller
	mChara->injectKeyDown(ke);

	// Check for the escape key
	if(ke.key == OIS::KC_ESCAPE)
	{
		// Set the flag to shut down the application
		mShutDown = true;
	}

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::keyReleased(const OIS::KeyEvent& ke)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::keyReleased(ke);

	// Relay keyboard input events to character controller
	mChara->injectKeyUp(ke);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mouseMoved(const OIS::MouseEvent& me)
{
	// Relay mouse input events to character controller
	mChara->injectMouseMove(me);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mousePressed(const OIS::MouseEvent& me, OIS::MouseButtonID id)
{
	// Relay mouse input events to character controller
	mChara->injectMouseDown(me, id);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mouseReleased(const OIS::MouseEvent& me, OIS::MouseButtonID id)
{
	// Do nothing

	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		} catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
			e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------